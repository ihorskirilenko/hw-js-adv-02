'use strict'

/**
 * Теорія:
 * Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
 * - Для можливості перехоплення та обробки помилок інтерпретатора, при виконанні некоректного коду.
 * - Для можливості генерації власних об'єктів помилок при досягненні певних умов.
 *
 *
 * Задача:
 * Дано масив books.
 * Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
 * На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
 * Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price).
 * Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
 * Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
 * */

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


class Book {

    constructor(author, name, price) {

        try {
            if (!author) {
                throw new Error(`Error, author undefined`);
            } else if (!name) {
                throw new Error(`Error, name undefined`);
            } else if (!price) {
                throw new Error(`Error, price undefined`);
            }

            this.author = author
            this.name = name
            this.price = price

        } catch (err) {
            console.log(err);
        }
    }

    render() {
        const parent = document.querySelector('#root');
        const wrapper = document.createElement('ul');

        for(let prop in this) {
            let elem = document.createElement('li');
            elem.innerText = this[prop];
            wrapper.append(elem);
        }
        parent.append(wrapper);
    }
}


for (let obj of books) {
    new Book(obj.author, obj.name, obj.price).render();
}